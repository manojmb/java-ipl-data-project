import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) throws IOException {
        String matchesPath = "src/data/matches.csv";
        String deliveriesPath = "src/data/deliveries.csv";
        String output = "src/output/";


        try {
            String[] csvMatchData = ReadAndWriteFile.readFileMatches(matchesPath);
            String[] csvDeliveriesData = ReadAndWriteFile.readFileMatches(deliveriesPath);
//            totalMatchesPerYear(csvMatchData, output + "totalMatchesPerYear.json");
//            matchesWonPerTeamPerYear(csvMatchData);
//            extraRunConcededPerTeamIn2016(csvMatchData, csvDeliveriesData);
//            top10EconomicPlayerIn2015(csvMatchData, csvDeliveriesData);
//            numberOfTimesWonMatchAndToss(csvMatchData);
            strikeRateOfBatsmanEachYear(csvMatchData, csvDeliveriesData);
//            highestPlayerOfTheMatch(csvMatchData);
        } catch (IOException e) {
            System.err.println("Error reading CSV file: " + e.getMessage());
        }
    }

    private static void strikeRateOfBatsmanEachYear(String[] csvMatchData, String[] csvDeliveriesData) {
        Map<String, Map<String, BatsmanStats>> batsmanRecord = new HashMap<>();
        Map<String, Set<String>> yearOfMatchId = new HashMap<>();
        for (String csvMatchDatum : csvMatchData) {
            String[] match = csvMatchDatum.split(",");
            yearOfMatchId.putIfAbsent(match[1], new HashSet<>());
            yearOfMatchId.get(match[1]).add(match[0]);
        }
        for (int index = 1; index < csvDeliveriesData.length; index++) {
            String[] delivery = csvDeliveriesData[index].split(",");
            String year = "";
            String matchId = delivery[0];
            String batsman = delivery[6];
            int runs = Integer.parseInt(delivery[15]);
            for (Map.Entry<String, Set<String>> match : yearOfMatchId.entrySet()) {
                if (match.getValue().contains(matchId)) {
                    year = match.getKey();
                    break;
                }
            }

            batsmanRecord.putIfAbsent(year, new HashMap<>());
            Map<String, BatsmanStats> yearRecord = batsmanRecord.get(year);
            yearRecord.putIfAbsent(batsman, new BatsmanStats(0, 0));
            BatsmanStats stats = yearRecord.get(batsman);
            stats.runs += runs;
            stats.balls += 1;
        }
        System.out.println(batsmanRecord);

    }

    private static void extraRunConcededPerTeamIn2016(String[] csvMatchData, String[] csvDeliveriesData) {
        Map<String, Integer> extraRuns = new TreeMap<>();
        ArrayList<Integer> idOf2016 = new ArrayList<>();
        for (int index = 1; index < csvMatchData.length; index++) {
            String[] match = csvMatchData[index].split(",");
            String id = match[0];
            String year = match[1];
            if (Objects.equals(year, "2016")) {
                idOf2016.add(Integer.parseInt(id));
            }
        }
        System.out.println(csvDeliveriesData[0].split(",")[16]);
        for (int index = 1; index < csvDeliveriesData.length; index++) {
            String[] match = csvDeliveriesData[index].split(",");
            if (Integer.parseInt(match[0]) >= idOf2016.getFirst() && Integer.parseInt(match[0]) <= idOf2016.getLast()) {
                extraRuns.put(match[2], extraRuns.getOrDefault(match[2], 0) + Integer.parseInt(match[16]));
            }

        }
        System.out.println(extraRuns);
    }


    private static void top10EconomicPlayerIn2015(String[] csvMatchData, String[] csvDeliveriesData) {
        Map<String, Integer> bowlerRuns = new HashMap<>();
        Map<String, Integer> bowlerBalls = new HashMap<>();

        ArrayList<Integer> idOf2015 = new ArrayList<>();
        for (int index = 1; index < csvMatchData.length; index++) {
            String[] match = csvMatchData[index].split(",");
            String id = match[0];
            String year = match[1];
            if (Objects.equals(year, "2015")) {
                idOf2015.add(Integer.parseInt(id));
            }
        }

        for (int index = 1; index < csvDeliveriesData.length; index++) {
            String[] eachBall = csvDeliveriesData[index].split(",");
            int totalRuns = Integer.parseInt(eachBall[17]);
            String bowler = eachBall[8];
            String matchId = eachBall[0];

            if (idOf2015.contains(Integer.parseInt(matchId))) {
                bowlerRuns.put(bowler, bowlerRuns.getOrDefault(bowler, 0) + totalRuns);
                bowlerBalls.put(bowler, bowlerBalls.getOrDefault(bowler, 0) + 1);
            }
        }
        System.out.println();
        Map<String, Double> bowlerEconomy = new HashMap<>();
        for (Map.Entry<String, Integer> entry : bowlerRuns.entrySet()) {
            String bowler = entry.getKey();
            int runs = entry.getValue();
            int balls = bowlerBalls.get(bowler);
            double economy = calculateEconomyRate(runs, balls);
            bowlerEconomy.put(bowler, economy);
        }


        String[] sortedBowlers = bowlerEconomy.keySet().toArray(new String[0]);
        Arrays.sort(sortedBowlers, Comparator.comparingDouble(bowlerEconomy::get));


        System.out.println("Top 10 Economical Bowlers of 2015:");
        for (int i = 0; i < 10; i++) {
//        for (int i = 0; i < Math.min(10, sortedBowlers.length); i++) {
            String bowler = sortedBowlers[i];
            double economy = bowlerEconomy.get(bowler);
            System.out.println((i + 1) + ". " + bowler + " - Economy: " + economy);
        }
    }

    private static double calculateEconomyRate(int runs, int balls) {

        if (balls == 0) {
            return Double.MAX_VALUE;
        }
        return ((double) runs / balls) * 6.0;
    }


    private static void totalMatchesPerYear(String[] csvMatchData, String outputFolderPath) {
        Map<String, Integer> totalMatches = new HashMap<>();

        for (int i = 1; i < csvMatchData.length; i++) {
            String[] match = csvMatchData[i].split(",");
            String year = match[1];
            totalMatches.put(year, totalMatches.getOrDefault(year, 0) + 1);
        }
        try {
            ReadAndWriteFile.formatJson(totalMatches, outputFolderPath);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private static void matchesWonPerTeamPerYear(String[] csvMatchData) {
        Map<String, Map<String, Integer>> totalMatchesWonPerTeam = new TreeMap<>();
        System.out.println(csvMatchData[0]);
        for (int i = 1; i < csvMatchData.length; i++) {
            String[] match = csvMatchData[i].split(",");
            String year = match[1];
            String winner = match[10];

            if (year != null && winner != null && !winner.isEmpty()) {
                totalMatchesWonPerTeam.putIfAbsent(year, new HashMap<>());

                totalMatchesWonPerTeam.get(year).put(winner, totalMatchesWonPerTeam.get(year).getOrDefault(winner, 0) + 1);
            }
        }


        System.out.println("Total Matches Won Per Team Per year");
        for (Map.Entry<String, Map<String, Integer>> matchesWon : totalMatchesWonPerTeam.entrySet()) {
            System.out.println(matchesWon);
        }

    }

    private static void numberOfTimesWonMatchAndToss(String[] csvMatchData) {
        Map<String, Integer> tossAndMatchWinner = new HashMap<>();

        for (int index = 1; index < csvMatchData.length; index++) {
            String[] match = csvMatchData[index].split(",");
            String tossWinner = match[6];
            String winner = match[10];
            if (!winner.isEmpty() && Objects.equals(winner, tossWinner)) {
                tossAndMatchWinner.put(winner, tossAndMatchWinner.getOrDefault(winner, 0) + 1);
            }

        }
        System.out.println(tossAndMatchWinner);
    }


    private static void highestPlayerOfTheMatch(String[] csvMatchData) {
        Map<String, Map<String, Integer>> playerOfMatchRecord = new HashMap<>();
        Map<String, Map<String, Integer>> playerOfMatchRecordEachYear = new HashMap<>();

        for (int index = 1; index < csvMatchData.length; index++) {
            String[] match = csvMatchData[index].split(",");
            String year = match[1];
            playerOfMatchRecord.putIfAbsent(year, new HashMap<>());
            String playerOfTheMatch = match[13];

            playerOfMatchRecord.get(year).put(playerOfTheMatch, playerOfMatchRecord.get(year).getOrDefault(playerOfTheMatch, 0) + 1);
        }

        for (Map.Entry<String, Map<String, Integer>> entry : playerOfMatchRecord.entrySet()) {
            int maxScore = 0;
            Map<String, Integer> maxPlayers = new HashMap<>();

            for (Map.Entry<String, Integer> playerEntry : entry.getValue().entrySet()) {
                int score = playerEntry.getValue();
                String player = playerEntry.getKey();

                if (score > maxScore) {
                    maxScore = score;
                    maxPlayers.clear(); // Clear previous maxPlayers
                    maxPlayers.put(player, score);
                } else if (score == maxScore) {
                    maxPlayers.put(player, score);
                }
            }
            playerOfMatchRecordEachYear.put(entry.getKey(), new HashMap<>(maxPlayers));
        }

        System.out.println(playerOfMatchRecordEachYear);
    }


}
