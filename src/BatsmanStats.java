import java.util.*;

public class BatsmanStats {
    int runs;
    int balls;

    public BatsmanStats(int runs, int balls) {
        this.runs = runs;
        this.balls = balls;
    }

    public double getStrikeRate() {
        return (balls != 0) ? (double) runs / balls * 100 : 0.0;
    }

    public String toString() {
        return "Runs: " + runs + ", Balls: " + balls + ", Strike Rate: " + getStrikeRate();
    }

}


