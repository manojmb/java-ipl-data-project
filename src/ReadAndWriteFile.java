import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class ReadAndWriteFile {
    static String[] readFileMatches(String path) throws IOException {
        String[] csvMatchData = {};

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            csvMatchData = br.lines().toArray(String[]::new);
        } catch (IOException e) {
            System.err.println("Error reading CSV file: " + e.getMessage());
            throw e;
        }
        return csvMatchData;
    }

    static void formatJson(Map<String, Integer> data, String outputPath) {
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");
        data.forEach((key, value) -> sb.append("  \"").append(key).append("\": ").append(value).append(",\n"));
        if (!data.isEmpty()) {
            sb.deleteCharAt(sb.length() - 2);  // Remove the trailing comma
        }
        sb.append("}\n");

        // Write the formatted JSON to the file
        try (FileWriter writer = new FileWriter(outputPath)) {
            writer.write(sb.toString());
        } catch (IOException e) {
            System.err.println("Error writing JSON file: " + e.getMessage());
        }

    }
}
